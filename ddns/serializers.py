from rest_framework import serializers
from .models import CFRecord


class CFRecordSerializer(serializers.ModelSerializer):
    z = serializers.CharField(source='zone')
    content = serializers.IPAddressField(protocol='IPv4', source='host')
    type = serializers.CharField(source='record_type')

    class Meta:
        model = CFRecord
        fields = ('name', 'z', 'content', 'type', 'ttl', 'service_mode',)
