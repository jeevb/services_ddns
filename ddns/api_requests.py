from base.api_requests import GetAPIRequest, PostAPIRequest
from .settings import ddns_settings


class ExternalIP(GetAPIRequest):
    base_url = 'http://my-ip.heroku.com/'
    endpoint = ''

    def response(self):
        return super(ExternalIP, self).response().json().get('ip', None)


class CloudFlareAPI(PostAPIRequest):
    base_url = 'https://www.cloudflare.com/'
    endpoint = 'api_json.html'

    def __init__(self,
                 api_call_args=None,
                 **kwargs):
        data = self._build_payload(**kwargs)
        super(CloudFlareAPI, self).__init__(data=data)

    def _build_payload(self, **kwargs):
        payload = {
            'tkn': ddns_settings.CF_TOKEN,
            'email': ddns_settings.CF_EMAIL,
            'a': self.get_action(),
        }
        payload.update(**kwargs)
        return payload

    def get_action(self):
        assert self.action is not None, (
            '%r should either include a `action` attribute, '
            'or override the `get_action()` action.'
            % self.__class__.__name__
        )

        return self.action


class CloudFlareAPIFetch(CloudFlareAPI):
    action = 'rec_load_all'

    def response(self):
        response = super(CloudFlareAPIFetch, self).response().json()
        return response['response']['recs'].get('objs', [])


class CloudFlareAPIUpdate(CloudFlareAPI):
    action = 'rec_edit'


class CloudFlareAPIDelete(CloudFlareAPI):
    action = 'rec_delete'


class CloudFlareAPICreate(CloudFlareAPI):
    action = 'rec_new'
