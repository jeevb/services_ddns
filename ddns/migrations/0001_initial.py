# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import base.mixins


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CFRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subdomain', models.CharField(max_length=50)),
                ('zone', models.CharField(max_length=255)),
                ('host', models.GenericIPAddressField()),
                ('record_type', models.CharField(default=b'A', max_length=10)),
                ('ttl', models.IntegerField(default=1)),
                ('service_mode', models.IntegerField(default=0)),
            ],
            options={
                'verbose_name': 'Cloudflare Record',
                'verbose_name_plural': 'Cloudflare Records',
            },
            bases=(base.mixins.TrackedModelMixin, models.Model),
        ),
    ]
