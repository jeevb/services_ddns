from django.db import models
from base.mixins import TrackedModelMixin


class CFRecord(TrackedModelMixin, models.Model):
    subdomain = models.CharField(max_length=50)
    zone = models.CharField(max_length=255)
    host = models.GenericIPAddressField()
    record_type = models.CharField(max_length=10, default='A')
    ttl = models.IntegerField(default=1)
    service_mode = models.IntegerField(default=0)

    # Track changes to instance
    untracked = ('active',)
    updated = False

    @property
    def name(self):
        return u'{subdomain}.{tld}'.format(subdomain=self.subdomain,
                                           tld=self.zone)

    def __repr__(self):
        return (
            u'<CFRecord Name: {name} Zone: {zone} Host: {host}>'.format(
                name=self.name,
                zone=self.zone,
                host=self.host,
            )
        )

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Cloudflare Record'
        verbose_name_plural = 'Cloudflare Records'
