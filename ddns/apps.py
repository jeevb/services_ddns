from django.apps import AppConfig

class DDNSConfig(AppConfig):
    name = 'ddns'
    verbose_name = 'DDNS'

    def ready(self):
        from .signals import post_save_cf_record, post_delete_cf_record
