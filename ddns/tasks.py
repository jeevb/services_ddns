from celery import shared_task
from .controller import Controller

@shared_task
def heartbeat():
    Controller().heartbeat()

@shared_task
def cf_update_or_create(pk):
    Controller().update_or_create(pk)

@shared_task
def cf_delete(zone, name):
    Controller().delete(zone, name)
