from django.contrib import admin
from .models import CFRecord


@admin.register(CFRecord)
class CFRecordAdmin(admin.ModelAdmin):
    list_display = ('subdomain', 'zone', 'host', 'record_type', 'ttl',
                    'service_mode',)
    list_filter = ('zone', 'record_type',)
    search_fields = ('subdomain', 'zone', 'host',)
