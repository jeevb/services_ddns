from .api_requests import (
    CloudFlareAPIFetch,
    CloudFlareAPIUpdate,
    CloudFlareAPIDelete,
    CloudFlareAPICreate,
)


class CloudFlareZone(object):
    def __init__(self, zone):
        super(CloudFlareZone, self).__init__()
        self._zone = zone
        self._records = CloudFlareAPIFetch(z=self._zone).response()

    def _get(self, name):
        for rec in self._records:
            if rec.get('name') == name:
                return rec

    def update_or_create(self, **kwargs):
        name = kwargs.get('name')
        rec = self._get(name)

        if rec:
            rec.update(kwargs)
            return CloudFlareAPIUpdate(id=rec.get('rec_id'), **rec).response()
        else:
            return CloudFlareAPICreate(**kwargs).response()

    def delete(self, name):
        rec = self._get(name)
        if rec:
            return CloudFlareAPIDelete(
                id=rec.get('rec_id'),
                z=self._zone
            ).response()
