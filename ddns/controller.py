import logging

from django.db import transaction
from django.db.models import Q
from .models import CFRecord
from .serializers import CFRecordSerializer
from .api_requests import ExternalIP
from .cloudflare import CloudFlareZone

logger = logging.getLogger(__name__)


class Controller(object):
    def __init__(self):
        super(Controller, self).__init__()
        self._zone_cache = {}

    @staticmethod
    def _serialize(cf_record, **kwargs):
        serialized = CFRecordSerializer(cf_record).data
        serialized.update(kwargs)

        return serialized

    def _get_zone(self, zone):
        if zone in self._zone_cache:
            return self._zone_cache[zone]

        cf_zone = self._zone_cache[zone] = CloudFlareZone(zone)
        return cf_zone

    def _update(self):
        # Retrieve external IP
        external_ip = ExternalIP().response()

        # Identify records that need to be updated
        update_queue = CFRecord.objects.filter(~Q(host=external_ip))

        # Push updates to Cloudflare
        for rec in update_queue:
            serialized = self._serialize(rec, content=external_ip)
            self._get_zone(rec.zone).update_or_create(**serialized)

        # Push updates to DB if Cloudflare update is successful
        update_queue.update(host=external_ip)

    def heartbeat(self):
        try:
            logger.info('Starting Cloudflare DDNS update...')
            with transaction.atomic():
                self._update()
        except Exception, e:
            logger.exception(e)
        else:
            logger.info('Cloudflare DDNS update successfully completed.')

    def update_or_create(self, pk):
        cf_record = CFRecord.objects.get(pk=pk)
        logger.info('Adding/Updating Cloudflare Record: %s' % repr(cf_record))

        serialized = self._serialize(cf_record)
        self._get_zone(cf_record.zone).update_or_create(**serialized)

    def delete(self, zone, name):
        logger.info('Deleting \'%s\' in zone: \'%s\'' % (name, zone))
        self._get_zone(zone).delete(name)
