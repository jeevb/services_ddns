from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from .models import CFRecord
from .tasks import cf_update_or_create, cf_delete

@receiver(post_save, sender=CFRecord)
def post_save_cf_record(sender, instance, *args, **kwargs):
    cf_update_or_create.delay(instance.pk)

@receiver(post_delete, sender=CFRecord)
def post_delete_cf_record(sender, instance, *args, **kwargs):
    cf_delete.delay(instance.zone, instance.name)
