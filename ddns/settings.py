from django.conf import settings
from django.dispatch import receiver
from django.test.signals import setting_changed
from base.settings import BaseAppSettings

USER_SETTINGS = getattr(settings, 'DDNS_SETTINGS', None)

DEFAULTS = {
    # CloudFlare settings
    'CF_TOKEN': '',
    'CF_EMAIL': '',
}


class DDNSSettings(BaseAppSettings):
    pass


ddns_settings = DDNSSettings(USER_SETTINGS, DEFAULTS)


@receiver(setting_changed)
def reload_ddns_settings(*args, **kwargs):
    global ddns_settings
    setting, value = kwargs['setting'], kwargs['value']
    if setting == 'DDNS_SETTINGS':
        ddns_settings = DDNSSettings(value, DEFAULTS)
